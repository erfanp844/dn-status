#!/bin/bash

set -e

if [ -z "$APP_NAME" ]; then
  APP_NAME="dn-status"
fi

if [ -z "$APP_DIR" ]; then
  APP_DIR="/opt/$APP_NAME"
fi

if [ -z "$LOG_DIR" ]; then
  LOG_DIR="/var/log/$APP_NAME"
fi

cd "$APP_DIR"

source venv/bin/activate

mkdir -p "$LOG_DIR"

python run.py >> "$LOG_DIR/logfile.log" 2>&1
