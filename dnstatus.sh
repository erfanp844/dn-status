#!/usr/bin/env bash

set -e

if [ -z "$APP_NAME" ]; then
  APP_NAME="dn-status"
fi

if [ -z "$APP_DIR" ]; then
  APP_DIR="/opt/$APP_NAME"
fi

if [ -z "$DATA_DIR" ]; then
  DATA_DIR="/var/lib/$APP_NAME"
fi

if [ -z "$REPOSITORY_URL" ]; then
  REPOSITORY_URL="https://gitlab.com/erfanp844/dn-status.git"
fi

if [ -z "$DNS" ]; then
  DNS=("178.22.122.100" "185.51.200.2")
fi

colorized_echo() {
  local color=$1
  local text=$2

  case $color in
  "red")
    printf "\e[91m${text}\e[0m\n"
    ;;
  "green")
    printf "\e[92m${text}\e[0m\n"
    ;;
  "yellow")
    printf "\e[93m${text}\e[0m\n"
    ;;
  "blue")
    printf "\e[94m${text}\e[0m\n"
    ;;
  "magenta")
    printf "\e[95m${text}\e[0m\n"
    ;;
  "cyan")
    printf "\e[96m${text}\e[0m\n"
    ;;
  *)
    echo "${text}"
    ;;
  esac
}

check_running_as_root() {
  if [ "$(id -u)" != "0" ]; then
    colorized_echo red "This command must be run as root."
    exit 1
  fi
}

detect_os() {
  if [ -z "$OS" ]; then
    # Detect the operating system
    if [ -f /etc/lsb-release ]; then
      OS=$(lsb_release -si)
    elif [ -f /etc/os-release ]; then
      OS=$(awk -F= '/^NAME/{print $2}' /etc/os-release | tr -d '"')
    elif [ -f /etc/redhat-release ]; then
      OS=$(cat /etc/redhat-release | awk '{print $1}')
    elif [ -f /etc/arch-release ]; then
      OS="Arch"
    else
      colorized_echo red "Unsupported operating system"
      exit 1
    fi
  fi
}

detect_and_update_package_manager_on_os() {
  detect_os
  colorized_echo blue "Updating package manager"
  if [[ "$OS" == "Ubuntu"* ]] || [[ "$OS" == "Debian"* ]]; then
    PKG_MANAGER="apt-get"
    $PKG_MANAGER update
  elif [[ "$OS" == "CentOS"* ]]; then
    PKG_MANAGER="yum"
    $PKG_MANAGER update -y
    $PKG_MANAGER epel-release -y
  elif [[ "$OS" == "Fedora"* ]]; then
    PKG_MANAGER="dnf"
    $PKG_MANAGER update
  elif [ "$OS" == "Arch" ]; then
    PKG_MANAGER="pacman"
    $PKG_MANAGER -Sy
  else
    colorized_echo red "Unsupported operating system"
    exit 1
  fi
}

change_dns_other() {

  colorized_echo blue "Changing DNS..."

  # Comment out or remove existing nameservers
  sed -i '/^nameserver/d' /etc/resolv.conf

  # Add new nameservers
  {
    printf "nameserver %s\n" "${DNS[@]}"
  } >> /etc/resolv.conf

  colorized_echo green "DNS changed successfully!"

}

change_dns_arch() {
  colorized_echo blue "Changing DNS on Arch OS..."

  # Comment out or remove existing nameservers
  sed -i '/^nameserver/d' /etc/resolve.conf

  # Add new nameservers
  {
    printf "nameserver %s\n" "${DNS[@]}"
  } >> /etc/resolve.conf

  colorized_echo green "DNS changed successfully!"
}

change_dns_based_on_os() {
  read -p "Do you want to Change DNS for better install (Recomended use in iran server)? (y/n) "
    if [[ $REPLY =~ ^[Yy]$ ]]; then
      detect_os
      if [[ "$OS" == "Ubuntu"* ]] || [[ "$OS" == "Debian"* ]]; then
        # Change DNS based on Ubuntu or Debian
        change_dns_other
      elif [[ "$OS" == "CentOS"* ]] || [[ "$OS" == "Fedora"* ]]; then
        # Change DNS based on CentOS or Fedora
        change_dns_other
      elif [ "$OS" == "Arch" ]; then
        # Change DNS based on Arch
        change_dns_arch
      else
        echo "Unsupported operating system"
        exit 1
      fi
    fi
}

install_package() {

  if [ -z $PKG_MANAGER ]; then
    detect_and_update_package_manager_on_os
  fi

  PACKAGE=$1
  colorized_echo blue "Installing $PACKAGE"
  if [[ "$OS" == "Ubuntu"* ]] || [[ "$OS" == "Debian"* ]]; then
    $PKG_MANAGER -y install "$PACKAGE"
  elif [[ "$OS" == "CentOS"* ]]; then
    $PKG_MANAGER install -y "$PACKAGE"
  elif [[ "$OS" == "Fedora"* ]]; then
    $PKG_MANAGER install -y "$PACKAGE"
  elif [ "$OS" == "Arch" ]; then
    $PKG_MANAGER -S --noconfirm "$PACKAGE"
  else
    colorized_echo red "Unsupported operating system"
    exit 1
  fi
}

install_docker() {
  # Install Docker and Docker Compose using the official installation script
  colorized_echo blue "Installing Docker"
  curl -fsSL https://get.docker.com | sh
  colorized_echo green "Docker installed successfully"

  colorized_echo yellow "requirements to change DNS after install Docker"
  change_dns_based_on_os
}

is_dn_status_installed() {
  if [ -d "$APP_DIR" ]; then
    return 0
  else
    return 1
  fi
}

install_dn_status() {
  # Install DigitalNetwork Status file from gitlab
  colorized_echo blue "Installing DigitalNetwork Status"
  if is_dn_status_installed; then
      rm -r $APP_DIR
  fi
  git clone $REPOSITORY_URL $APP_DIR
  colorized_echo green "DigitalNetwork Status installed successfully"
}

preparation_dn_status() {
  # preparation for DigitalNetwork Status
  colorized_echo blue "Preparing DigitalNetwork Status"
  cd $APP_DIR
  colorized_echo yellow "Install venv (virtual environment) and activate it ..."
  python3 -m venv venv
  source venv/bin/activate
  colorized_echo green "Done... , venv created successfully and active it."
  colorized_echo yellow "Install requirements packages ..."
  pip install --upgrade pip
  pip install -r requirements.txt
  colorized_echo green "Done... , requirements installed!"
  colorized_echo yellow "Create .env file ..."
  mv .env.example .env
  colorized_echo green ".env file was created for edit setting change it."
  colorized_echo green "DigitalNetwork Status preparation completed successfully"
}

start_command() {
  check_running_as_root
  # Check dn status installed and service start
  if ! is_dn_status_installed; then
    colorized_echo red "DigitalNetwork Status not installed , please install this first and use this command."
    exit 0
  fi

  systemctl start dn_status

  colorized_echo green "DigitalNetwork status started!"
}

stop_command() {
  check_running_as_root
  # Check dn status installed and service stop
  if ! is_dn_status_installed; then
    colorized_echo red "DigitalNetwork Status not installed , please install this first and use this command."
    exit 0
  fi

  systemctl stop dn_status

  colorized_echo green "DigitalNetwork status stoped!"
}

restart_command() {
  check_running_as_root
  # Check dn status installed and service restart
  if ! is_dn_status_installed; then
    colorized_echo red "DigitalNetwork Status not installed , please install this first and use this command."
    exit 0
  fi

  systemctl daemon-reload
  systemctl restart dn_status

  colorized_echo green "DigitalNetwork status restarted!"
}

status_command() {
  check_running_as_root
  # Check dn status installed and service status
  if ! is_dn_status_installed; then
    colorized_echo red "DigitalNetwork Status not installed , please install this first and use this command."
    exit 0
  fi

  systemctl status dn_status
}

create_dn_status_service() {

  # Create DigitalNetwork Status service

  chmod +x "$APP_DIR/run.sh"

  colorized_echo blue "Creating DigitalNetwork Status service"
  cat <<EOF >/etc/systemd/system/dn_status.service
    [Unit]
    Description=DigitalNetwork monitoring app service
    After=multi-user.target
    [Service]
    Type=simple
    Restart=always
    ExecStart=$APP_DIR/run.sh
    [Install]
    WantedBy=multi-user.target
EOF

  systemctl daemon-reload
  systemctl enable dn_status
  start_command

  colorized_echo green "DigitalNetwork Status service created successfully and start it ..."
}

logs_command() {
  check_running_as_root
  # Check dn status installed and service status
  if ! is_dn_status_installed; then
    colorized_echo red "DigitalNetwork Status not installed , please install this first and use this command."
    exit 0
  fi

  cd $APP_DIR
  python3 cli.py logs
}

install_dn_cli() {
  cp "$APP_DIR/dnstatus.sh" "/usr/local/bin/dnstatus"
  chmod +x "/usr/local/bin/dnstatus"

  rm -r "$APP_DIR/dnstatus.sh"
}

install_command() {
  check_running_as_root
  change_dns_based_on_os
  # Check if digitalnetwork is already installed
  if is_dn_status_installed; then
    colorized_echo red "DigitalNetwork is already installed at $APP_DIR"
    read -p "Do you want to override the previous installation? (y/n) "
    if [[ ! $REPLY =~ ^[Yy]$ ]]; then
      colorized_echo red "Aborted installation"
      exit 1
    fi
  fi
  if ! command -v jq >/dev/null 2>&1; then
    install_package jq
  fi
  if ! command -v curl >/dev/null 2>&1; then
    install_package curl
  fi
  if ! command -v python3-venv >/dev/null 2>&1; then
    install_package python3-venv
  fi
  if ! command -v python3 >/dev/null 2>&1; then
    install_package python3
  fi
  if ! command -v pip3 >/dev/null 2>&1; then
    install_package python3-pip
  fi
  if ! command -v docker >/dev/null 2>&1; then
    install_docker
  fi
  install_dn_status
  preparation_dn_status
  create_dn_status_service
  install_dn_cli
}

update_command() {
  check_running_as_root
  # Check if digitalnetwork is already installed
  colorized_echo green "Your app is already update.!"
}

uninstall_command() {
  check_running_as_root
  # Check if digitalnetwork is already installed
  read -p "Are you sure to uninstall digitalnetwork status? (y/n) "
  if [[ ! $REPLY =~ ^[Yy]$ ]]; then
    colorized_echo red "Aborted installation"
    exit 1
  fi

  systemctl stop dn_status
  systemctl disable dn_status

  colorized_echo green "Stop and disable service"

  rm /etc/systemd/system/dn_status.service

  colorized_echo green "dn_status service deleted!"

  rm -r $APP_DIR

  colorized_echo green "DigitalNetwork Status app deleted!"

}

case "$1" in
start)
  shift
  start_command "$@"
  ;;
stop)
  shift
  stop_command "$@"
  ;;
restart)
  shift
  restart_command "$@"
  ;;
status)
  shift
  status_command "$@"
  ;;
logs)
  shift
  logs_command "$@"
  ;;
install)
  shift
  install_command "$@"
  ;;
update)
  shift
  update_command "$@"
  ;;
uninstall)
  shift
  uninstall_command "$@"
  ;;
*)
  usage
  ;;
esac
