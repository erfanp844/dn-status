import os
import time

from docker import DockerClient
from docker.errors import ContainerError, APIError
from datetime import datetime
from colorama import Fore, Back, Style
from config import DOCKER_CLIENT_BASE_URL, XRAY_IMAGE_NAME


class XRayDocker:

    client = DockerClient(base_url=DOCKER_CLIENT_BASE_URL)

    def __init__(self, server_id, container_name, config_file):
        self.container_id = server_id
        self.container_name = container_name
        self.container_image = XRAY_IMAGE_NAME
        self.config_file = config_file
        self.container_port = None
        self.container = None

        self.start_xray_container()

    def __exit__(self):
        self.stop_xray_container()
        self.remove_xray_container()

    def check_status(self):
        if self.container_name != "":
            return self.client.containers.get(self.container_name).status

    def start_xray_container(self):
        if self.container_id <= 9:
            self.container_port = int("1080%s" % self.container_id)
        else:
            self.container_port = int("108%s" % self.container_id)

        try:
            self.container = self.client.containers.run(
                image=self.container_image,
                name=self.container_name,
                volumes={f"{self.config_file}": {"bind": "/etc/xray/config.json", "mode": "rw"}},
                ports={
                    "9090/tcp": ("127.0.0.1", str(self.container_port)),
                    "9090/udp": ("127.0.0.1", str(self.container_port))
                },
                privileged=True,
                detach=True
            )
            self.container.reload()
        except ContainerError as error:
            print(Fore.BLUE, "Pashmat", error)

        except APIError as error:
            print(Style.RESET_ALL)
            print(Fore.RED, "Error when run Container", error)

    def stop_xray_container(self):
        if self.container_name != "":
            self.container.stop()

    def remove_xray_container(self):
        if self.container_name != "":
            self.container.remove()

    def execute_command(self, command):
        if self.container_name != "":
            return self.container.exec_run(command)

    def logs(self):
        print(self.check_status)
        if self.check_status:
            try:
                for live_logs in self.container.logs(stream=True):
                    print(live_logs.strip())
            except:
                print("\nClose")
            finally:
                self.stop_xray_container()
                self.remove_xray_container()
