import platform

OS = {"WINDOWS": "WINDOWS", "LINUX": "LINUX", "MAC": "MAC", "UNKNOWN": "UNKNOWN"}


def detect_os():
    os_type = platform.system()
    if os_type == "Windows":
        return OS["WINDOWS"]
    elif os_type == "Linux":
        return OS["LINUX"]
    elif os_type == "Darwin":
        return OS["MAC"]
    else:
        return OS["UNKNOWN"]
