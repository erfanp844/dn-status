import asyncio
import asyncssh


class SSHConnection:
    def __init__(self, hostname, ssh_path, username="root", port=22, timeout=10):
        self.hostname = hostname
        self.ssh_path = ssh_path
        self.username = username
        self.port = port
        self.timeout = timeout
        self.connection = None

    async def connect(self):
        """
        Establish the SSH connection.
        """
        try:
            self.connection = await asyncssh.connect(
                self.hostname,
                port=self.port,
                username=self.username,
                client_keys=[self.ssh_path],
                known_hosts=None,  # Skipping known_hosts check (customize as needed)
                # timeout=self.timeout
            )
            print(f"Connected to {self.hostname}")
        except (asyncssh.Error, OSError) as e:
            print(f"Failed to connect to {self.hostname}: {e}")
            self.connection = None

    async def disconnect(self):
        """
        Close the SSH connection.
        """
        if self.connection:
            self.connection.close()
            await self.connection.wait_closed()
            print(f"Disconnected from {self.hostname}")
        else:
            print("No active connection to close.")

    async def exec_command(self, command):
        """
        Execute a command on the remote server.

        :param command: Command to execute
        :return: Command output (stdout)
        """
        if not self.connection:
            raise ConnectionError("SSH connection is not established. Call connect() first.")

        try:
            result = await self.connection.run(command, check=True)
            print(f"Command executed successfully: {command}")
            return result.stdout
        except asyncssh.ProcessError as e:
            print(f"Error executing command: {e}")
            return e.stderr




