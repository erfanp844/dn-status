import requests


class TelegramLogger:
    def __init__(self, bot_token, chat_id):
        """
        Initialize the logger with bot token and chat ID.
        :param bot_token: Telegram Bot API token
        :param chat_id: Chat ID to send messages to
        """
        self.bot_token = bot_token
        self.chat_id = chat_id
        self.api_url = f"https://api.telegram.org/bot{bot_token}/sendMessage"

    def send_message(self, message):
        """
        Send a text message to the specified chat.
        :param message: The message to send
        """
        payload = {
            'chat_id': self.chat_id,
            'text': message
        }
        try:
            response = requests.post(self.api_url, json=payload)
            if response.status_code == 200:
                print("Message sent successfully")
            else:
                print(f"Failed to send message: {response.text}")
        except Exception as e:
            print(f"Error: {e}")

    def send_log(self, log_message):
        """
        Send a log message to the chat.
        :param log_message: The log message to send
        """
        self.send_message(f"[LOG] {log_message}")
