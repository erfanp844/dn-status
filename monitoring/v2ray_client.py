import sys
import os
import time
import json
import base64
import requests
import multiprocessing
import asyncio
import json

from pathlib import Path
from tools.v2tj import convert_uri_json, LOG_LEVEL
from xray.docker import XRayDocker
from tools.ssh_connction import SSHConnection
from tools.telegram_bot import TelegramLogger
from config import TELEGRAM_LOGGER_BOT_TOKEN, TELEGRAM_LOGGER_USER_ID, PING_TARGET


class V2RayClient(multiprocessing.Process):
    def __init__(self, server_id, server_name, subscription_link, ip_foreign, ip_iran):
        super().__init__()
        self.server_id = server_id
        self.server_name = server_name
        self.subscription_link = subscription_link
        self.ip_foreign = ip_foreign
        self.ip_iran = ip_iran
        self.configs = None
        self.xray = None
        self.is_running = multiprocessing.Event()
        self.is_running.set()

        self.logger = TelegramLogger(TELEGRAM_LOGGER_BOT_TOKEN, TELEGRAM_LOGGER_USER_ID)

    def get_subscription_data(self):
        try:
            response_sub = requests.get(self.subscription_link)
            if response_sub.status_code == 200:
                subscription_data = response_sub.text
                encoded_data = base64.b64decode(subscription_data.encode('utf-8')).decode('utf-8')
                configs = encoded_data.split("\n")
                self.configs = configs
            else:
                print(f"{self.server_name}: Failed to retrieve subscription data")

        except requests.exceptions.RequestException as e:
            print('cant get subscription data')

    def connect_to_v2ray(self):
        config_file_path = convert_uri_json(
            host="0.0.0.0",
            socksport=9090,
            loglevel=LOG_LEVEL["DEBUG"],
            uri=self.configs[0]
        )
        self.xray = XRayDocker(server_id=self.server_id, container_name=self.server_name, config_file=config_file_path)
        # self.xray.logs()

    def ping_server(self, count=1, ping_website=PING_TARGET):

        ping_result = {}
        proxy = {
            'https': "socks5://127.17.0.1:%s" % self.xray.container_port
        }

        for repeat_count in range(count):
            global response
            try:
                start_time = time.time()
                response = requests.get(ping_website, proxies=proxy, timeout=10)
                end_time = time.time()
                ping_time = end_time - start_time
            except:
                ping_time = 0

            ping_result[repeat_count + 1] = (ping_time, response)

        ping_result_list = [result[0] for result in ping_result.values()]

        min_ping = round(min(ping_result_list), 4)
        max_ping = round(max(ping_result_list), 4)
        avg_ping = round(sum(ping_result_list) / len(ping_result_list), 4)

        return ping_result, min_ping, max_ping, avg_ping

    def cleanup(self):
        self.xray.stop_xray_container()
        self.xray.remove_xray_container()

    async def restart_marzban(self):
        ROOT_DIR = Path(sys.path[0]).resolve()
        ssh_key_path = ROOT_DIR / "keys" / "hetzener"

        ssh = SSHConnection(self.ip_foreign, ssh_key_path)
        await ssh.connect()

        try:
            output = await ssh.exec_command("docker inspect marzban-marzban-1")
            data = json.loads(output)
            if data[0]['State']['Running']:
                restart_out = await ssh.exec_command("marzban restart")
                print("marzban restarted: ", restart_out)
                self.logger.send_log(f"{self.server_name} : Marzban restarted")
                time.sleep(5)
                await ssh.disconnect()

        except ConnectionError as e:
            print(e)

            # Disconnect
        await ssh.disconnect()

    def run(self):
        self.get_subscription_data()
        self.connect_to_v2ray()

        while self.is_running.is_set():
            try:
                src, min_ping, max_ping, avg_ping = self.ping_server(count=5)
                print('src: ', src)
                print("min ping: ", min_ping * 1000, " ms")
                print("max ping: ", max_ping * 1000, "ms")
                print("avg_ping: ", avg_ping * 1000, " ms")

                if min_ping == 0:
                    print("has 0 ping in result")
                    self.logger.send_log(f"{self.server_name} : Has 0 ping in result")
                    asyncio.run(self.restart_marzban())
                if max_ping > 1:
                    print("max ping is very high")
                    self.logger.send_log(f"{self.server_name} : Max ping is very high : {max_ping}")
                    asyncio.run(self.restart_marzban())
                if avg_ping > 1:
                    print("ping is very high")
                    self.logger.send_log(f"{self.server_name} : Ping is very high : {avg_ping}")
                    asyncio.run(self.restart_marzban())

                time.sleep(60)
            except KeyboardInterrupt:
                self.cleanup()
                exit(0)
