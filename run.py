import signal
from monitoring.v2ray_client import V2RayClient
from config import SERVER_DATA

# Create an object for each server
if __name__ == '__main__':
    num_processes = len(SERVER_DATA)
    v2ray_clients = []

    for data in SERVER_DATA:
        client = V2RayClient(**data)

        v2ray_clients.append(client)
        client.start()

    for task in v2ray_clients:
        task.join()
