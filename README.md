# DN-Status



## Getting started

This project for monitoring and check status DigitalNetwork xray servers.

Some of the tasks that this program does :

* Check pinging each server at a specific time interval
* Measuring the upload and download speed of each server at a specific time interval
* Notifying the specified person in the project config about server outages
* Necessary actions such as server reset and attempts to reconnect servers

## Automatic Installation

```
sudo bash -c "$(wget -qO- https://gitlab.com/erfanp844/dn-status/raw/main/dnstatus.sh)" @ install
```