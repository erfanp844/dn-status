import os
from dotenv import load_dotenv

load_dotenv()

# Debug
DEBUG = os.environ.get('DEBUG', False)

# Docker
DOCKER_CLIENT_BASE_URL = os.environ.get('DOCKER_CLIENT_BASE_URL', 'unix://var/run/docker.sock')
XRAY_IMAGE_NAME = os.environ.get('XRAY_IMAGE_NAME', 'ghcr.io/xtls/xray-core:latest')

# Ping
PING_TARGET = os.environ.get('PING_TARGET', 'http://www.google.com/generate_204')

# Logger
TELEGRAM_LOGGER_BOT_TOKEN = os.environ.get('TELEGRAM_LOGGER_BOT_TOKEN')
TELEGRAM_LOGGER_USER_ID = os.environ.get('TELEGRAM_LOGGER_USER_ID')

# Server data

SERVER_DATA = [
    {
        "server_id": 1,
        "server_name": "VT1",
        "subscription_link": "https://vt1.subdigital.network:2087/sub/VlQxLDE3MjQ4ODEwNzUWZfY7e3ofx",
        "ip_foreign": "65.109.176.241",
        "ip_iran": "5.6.7.8"
    },
    {
        "server_id": 2,
        "server_name": "VT2",
        "subscription_link": "https://vt2.subdigital.network:2087/sub/VnQyTmV3LDE3MjQ4ODExMjYCMFqW4Yb2t",
        "ip_foreign": "65.109.182.177",
        "ip_iran": "5.6.7.8"
    },
    {
        "server_id": 3,
        "server_name": "VT3",
        "subscription_link": "https://vt3.subdigital.network:2087/sub/VnQzTmV3LDE3MzQyMTAzNjU14WXrV_H9w",
        "ip_foreign": "65.109.209.46",
        "ip_iran": "5.6.7.8"
    },
    {
        "server_id": 4,
        "server_name": "VT4",
        "subscription_link": "https://vt4.subdigital.network:2087/sub/VnQ0TmV3LDE3MzQyMTA0NzIoI36fUg0Rg",
        "ip_foreign": "65.109.181.159",
        "ip_iran": "5.6.7.8"
    },
    {
        "server_id": 5,
        "server_name": "VT5",
        "subscription_link": "https://vt5.subdigital.network:2087/sub/VnQ1TmV3LDE3MzQyMTA0OTgwEPwN4syhy",
        "ip_foreign": "65.109.183.123",
        "ip_iran": "5.6.7.8"
    },
    {
        "server_id": 6,
        "server_name": "VT6",
        "subscription_link": "https://vt6.subdigital.network:2087/sub/VnQ2TmV3LDE3MzQyMTA1NDkfyOrmDP9wF",
        "ip_foreign": "91.107.254.97",
        "ip_iran": "5.6.7.8"
    },
    {
        "server_id": 7,
        "server_name": "VT7",
        "subscription_link": "https://vt7.subdigital.network:2087/sub/VnQ3TmV3LDE3MzQyMTA1ODIWl0PJZyGQC",
        "ip_foreign": "91.107.242.174",
        "ip_iran": "5.6.7.8"
    },
    {
        "server_id": 8,
        "server_name": "VT8",
        "subscription_link": "https://vt8.subdigital.network:2087/sub/VnQ4TmV3LDE3MzQyMTA2Mzgxs9V_a2tGf",
        "ip_foreign": "5.75.203.243",
        "ip_iran": "5.6.7.8"
    },
    {
        "server_id": 9,
        "server_name": "VT9",
        "subscription_link": "https://vt9.subdigital.network:2087/sub/VnQ5TmV3LDE3MzQyMTA2OTIzEtXrDXodN",
        "ip_foreign": "91.107.146.134",
        "ip_iran": "5.6.7.8"
    },
    {
        "server_id": 10,
        "server_name": "VT10",
        "subscription_link": "https://vt10.subdigital.network:2087/sub/VlQxMCwxNzM0MjEwNzMzAF83TgIuPh",
        "ip_foreign": "91.107.150.18",
        "ip_iran": "5.6.7.8"
    }
]
